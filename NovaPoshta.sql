-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema NovaPoshta
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema NovaPoshta
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `NovaPoshta` DEFAULT CHARACTER SET utf8 ;
USE `NovaPoshta` ;

-- -----------------------------------------------------
-- Table `NovaPoshta`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`post_office`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`post_office` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `street` VARCHAR(45) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_post_office_city1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_post_office_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `NovaPoshta`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NULL,
  `mobile_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`cargo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`cargo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `weight` DECIMAL(10) NULL,
  `type` VARCHAR(45) NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_cargo_client1_idx` (`client_id` ASC) VISIBLE,
  CONSTRAINT `fk_cargo_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `NovaPoshta`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`courier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`courier` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`operator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`operator` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `post_office_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_operator_post_office1_idx` (`post_office_id` ASC) VISIBLE,
  CONSTRAINT `fk_operator_post_office1`
    FOREIGN KEY (`post_office_id`)
    REFERENCES `NovaPoshta`.`post_office` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`operator_has_client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`operator_has_client` (
  `operator_id` INT NOT NULL,
  `client_id` INT NOT NULL,
  PRIMARY KEY (`operator_id`, `client_id`),
  INDEX `fk_operator_has_client_client1_idx` (`client_id` ASC) VISIBLE,
  INDEX `fk_operator_has_client_operator1_idx` (`operator_id` ASC) VISIBLE,
  CONSTRAINT `fk_operator_has_client_operator1`
    FOREIGN KEY (`operator_id`)
    REFERENCES `NovaPoshta`.`operator` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_operator_has_client_client1`
    FOREIGN KEY (`client_id`)
    REFERENCES `NovaPoshta`.`client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `NovaPoshta`.`delivery`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `NovaPoshta`.`delivery` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cargo_id` INT NOT NULL,
  `courier_id` INT NOT NULL,
  `delivery_date` DATE NOT NULL,
  `post_office_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_delivery_cargo1_idx` (`cargo_id` ASC) VISIBLE,
  INDEX `fk_delivery_courier1_idx` (`courier_id` ASC) VISIBLE,
  INDEX `fk_delivery_post_office1_idx` (`post_office_id` ASC) VISIBLE,
  CONSTRAINT `fk_delivery_cargo1`
    FOREIGN KEY (`cargo_id`)
    REFERENCES `NovaPoshta`.`cargo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_courier1`
    FOREIGN KEY (`courier_id`)
    REFERENCES `NovaPoshta`.`courier` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_delivery_post_office1`
    FOREIGN KEY (`post_office_id`)
    REFERENCES `NovaPoshta`.`post_office` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


#----------insertions
use novaposhta;
#----------courier
insert into courier values('1', 'Ivan');
insert into courier values('2', 'Vasyl');
insert into courier values('3', 'Petro');
insert into courier values('4', 'Misha');
insert into courier values('5', 'Viktor');
#----------cities
insert into city values('1', 'Lviv');
insert into city values('2', 'Kyiv');
insert into city values('3', 'Dnipro');
insert into city values('4', 'Rivne');
insert into city values('5', 'Vinnytsia');
#-----------post-office
insert into post_office values('1','Luganska 14','1');
insert into post_office values('2','Lukasha 2','2');
insert into post_office values('3','Veresa 50 14','3');
insert into post_office values('4','Poroha 69','4');
insert into post_office values('5','Pushkina 12 ','5');
insert into post_office values('6','Bandery 14','1');
#-----------client
insert into client values('1','Petro','Shybka','+380938202540');
insert into client values('2','Ivan','Strila','+380968242540');
insert into client values('3','Shaitan','Banan','+380688207640');
insert into client values('4','Jagudron','Nagibovich','+380678207530');
insert into client values('5','Barbara','Straizen','+380508876450');
insert into client values('6','Adolf','Fabri','+380969556798');
#------------operator
insert into operator values('1','Vira','1');
insert into operator values('2','Snizhanna','1');
insert into operator values('3','Marusya','2');
insert into operator values('4','Dmytro','3');
insert into operator values('5','Alina','4');
insert into operator values('6','Jesika','4');
insert into operator values('7','Moysha','5');




